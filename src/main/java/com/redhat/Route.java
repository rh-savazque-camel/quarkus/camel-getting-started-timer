package com.redhat;

import org.apache.camel.builder.RouteBuilder;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Route extends RouteBuilder{
    @Override
    public void configure() throws Exception {
        from("timer:generate")
        .log("Hello router!");
    }
}
