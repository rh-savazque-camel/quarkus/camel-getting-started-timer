# Camel Getting Started

El proposito de este repositorio es tener una referencia de como generar un ruta de prueba de Apache Camel con Quarkus. Para este ejmplo, se utilizó el generador de proyectos de Quarkus:
    https://code.quarkus.io/

Y se instaló la siguiente extensión:
- Camel Timer [camel-quarkus-timer]

Tambien se puede instalar la extensión con el siguiente comando sobre el proyecto:
```shell script
mvn quarkus:add-extension -Dextensions="camel-quarkus-timer"
```

## Tecnologias utilizadas:
- Apache Maven 3.9.1
- Java 17 (OpenJDK 17.0.9)
- Camel Timer 3.6.0

## Acciones

### Levantar el modo de desarrollo:
```shell script
mvn compile quarkus:dev
```
![Screenshot](assets/readme_images/route_running.png)

<br>
<br>
<br>
<br>

# Documentación General de Quarkus

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/camel-quarkus-timer-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- Camel Timer ([guide](https://camel.apache.org/camel-quarkus/latest/reference/extensions/timer.html)): Generate messages in specified intervals using java.util.Timer
